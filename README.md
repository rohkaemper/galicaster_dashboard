# Galicaster Dashboard

## Installation
To install the Dashboard App you can clone the repository, install the requirements included and optionally set-up an HTTP Server like [Nginx][Optional: Set-up Nginx as a server].
```bash
# pull the repository and set permissions
git clone git@bitbucket.org:rohkaemper/galicaster_dashboard.git
cd galicaster_dashboard/
chmod +x ./manage.py

# install the requirements from file
pip install -r requirements.txt
```
### Initialize the Database
To initialize the database (db) you need to execute the following commands. After upgrades you can omit the "db init".
```python
# only for first run
./manage.py db init

# to migrate changes after updates
./manage.py db migrate
./manage.py db upgrade
```
### Adding an Admin Account to your DB
If you haven't done it already you can execute the following code to add an superuser account. Default values are **admin**/**admin**.
```python
# brackets are optional parameters:
./manage.py add_su [-u/--username "Admin"][-e/--email "admin"][-p/--password "admin"]
```

For the login process a valid email and password combination must be submitted.
If you want to change default admin/admin login, you can submit your customized information in quotes. You can change the details later in the Administration Tab, see section for the [Administration Tab](Administration (Admin only for direct DB access))

### Other important steps
Prerequisite to use all functionality of the Dashboard is that you allow a passwordless access to you Galicaster Units. To make that possible you need to have openssh installed and a certificate must be generated.
#### Add passwordless ssh access to the units
```bash
# Hint: If you are asked to type a password just hit **Enter**.
ssh-keygen
ssh-copy-id -i ~/.ssh/id_rsa.pub [user@]<remote-host>
# check with
ssh [user@]<remote-host>
```
#### Add passwordless option for reboot


## The Views
This section describes all the sections of the Dashboard Application.
### Dashboard Tab (all users)

### Configuration Tab (> Moderator)

### Administration (Admin only for direct DB access) ###
#### The Roles
The roles used in Galicaster Dashboard have different Permission Sets which will be described shortly in their section. The following Roles exist:
1. [User]()
2. [Adv. User]()
3. [Moderator]()
4. [Administrator]()
5. [Anonymous User]()
##### User
Bla
##### Adv. User
Blubs
##### Moderator
ModBla
##### Administrator
AdminBlubs
##### Anonymous User
The Anonymous User can only see the overview and refresh the information. He cannot access any other functions.

### Optional: Set-up Nginx as a server ###
