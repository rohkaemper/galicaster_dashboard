#!/usr/bin/env python
import os
from app import (create_app, db)
from flask_script import (Manager, Shell, prompt_bool)
from flask_migrate import (Migrate, MigrateCommand)
app = create_app(os.getenv('FLASK_CONFIG') or 'default')
manager = Manager(app)
migrate = Migrate(app, db)


@manager.option('-h', '--host', dest='host', default='0.0.0.0')
@manager.option('-p', '--port', dest='port', type=int, default=8081)
@manager.option('-w', '--workers', dest='workers', type=int, default=3)
def gunicorn(host, port, workers):
    """Start the Server with Gunicorn"""
    from gunicorn.app.base import Application

    class FlaskApplication(Application):

        def init(self, parser, opts, args):
            return {
                'bind': '{0}:{1}'.format(host, port),
                'workers': workers
            }

        def load(self):
            return app

    application = FlaskApplication()
    return application.run()


@manager.command
def test():
    import unittest2 as unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)


def make_shell_context():
    return dict(app=app, db=db, manager=manager, migrate=migrate)


@manager.option('-u', '--username', dest='username', default='Admin')
@manager.option('-e', '--email', dest='email', default='admin')
@manager.option('-p', '--password', dest='password', default='admin')
def add_su(username, email, password):
    from app.models import User, Role, Permission
    admin_exists = Role.query.filter_by(name='Administrator').first()
    user_exists = User.query.filter_by(username=username).first()

    if admin_exists is None:
        role_user = Role(
            name='User', default=True, permissions=(Permission.CONTROL_REC))
        role_adv_user = Role(name='Adv. User', default=False, permissions=(
            Permission.CONTROL_REC | Permission.REBOOT))
        role_mod = Role(
            name='Moderator', default=False, permissions=(Permission.MODERATOR))
        role_adm = Role(
            name='Admin', default=False, permissions=(Permission.ADMIN))
        roles = [role_user, role_adv_user, role_mod, role_adm]
        db.session.add_all(roles)
    else:
        role_adm = admin_exists

    if user_exists is None:
        su = User(username=username, email=email, password=password, role=role_adm)
        db.session.add(su)
        db.session.commit()
        print("Successfully added user!")
    else:
        print("Error -> User exists.")


@manager.option('-n', '--name', dest='name', default='TEST')
@manager.option('-i', '--ip', dest='ip', default='10.10.10.10')
@manager.option('-p', '--port', dest='port', type=int, default=8080)
@manager.option('-u', '--user', dest='user', default='galicaster')
def add_asset(name, ip, port, user):
    from app.models import Galicaster
    entry_exists = Galicaster.query.filter_by(name='TEST').first()

    if entry_exists is None:
        asset = Galicaster(name=name, ip=ip, port=port, user=user)
        db.session.add(asset)
        db.session.commit()
        print("Success! And don't forget to copy the ssh key to your galicaster instances...")
    else:
        print("Error: Asset already exists in db!")


manager.add_command('shell', Shell(make_context=make_shell_context))
manager.add_command('db', MigrateCommand)


if __name__ == '__main__':
    manager.run()
