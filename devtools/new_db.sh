#!/bin/bash

echo "Initiating Database..."
./manage.py db init
./manage.py db upgrade
./manage.py db migrate

echo "Adding default su: admin/admin"
./manage.py add_su

echo "Adding test entry!"
./manage.py add_asset

echo "done."
