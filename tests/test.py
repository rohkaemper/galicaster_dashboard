import unittest2 as unittest

from flask import current_app
from app import create_app, db


class BasicTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def testAppExists(self):
        self.assertFalse(current_app is None)

    def testAppIsTesting(self):
        self.assertTrue(current_app.config['TESTING'])
