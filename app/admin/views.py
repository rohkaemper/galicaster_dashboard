from flask import redirect, url_for, request
from flask_admin.contrib.sqla import ModelView
from flask_admin.menu import MenuLink
from flask_login import current_user

from ..models import User, Role, Galicaster
from .. import admin_page, db


class BasicView(ModelView):

    def is_accessible(self):
        return current_user.is_administrator

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(url_for('auth.login', next=request.url))


class UserView(BasicView):
    def on_model_change(self, form, User):
        User.password = form.password_hash.data


admin_page.add_view(UserView(User, db.session))
admin_page.add_view(BasicView(Role, db.session))
admin_page.add_view(BasicView(Galicaster, db.session))
admin_page.add_link(
    MenuLink(name='Leave Admin Page', category='', endpoint='main.index'))
