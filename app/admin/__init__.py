from flask import Blueprint, redirect, url_for
from flask_login import current_user

admin = Blueprint('adm', __name__)

from . import views
from ..main import main
from ..models import User, Role


@admin.before_request
def restrict_to_admins():
    if not current_user.is_administrator:
        return redirect(url_for('auth.login'))
