""" Galicaster Unit - Configuration """
from flask import current_app as app
from werkzeug.security import (generate_password_hash, check_password_hash)
from flask_login import UserMixin
from . import db, login_manager


assets = db.Table('assets',
                  db.Column('user_id', db.Integer,
                            db.ForeignKey('users.id')),
                  db.Column('galicaster_id', db.Integer,
                            db.ForeignKey('galicaster.id'))
                  )


class Permission:
    CONTROL_REC = 0x01
    REBOOT = 0x02
    ALTER_CONFIG = 0x04
    MODERATOR = 0x07
    ADMIN = 0xff


class Galicaster(db.Model):
    __tablename__ = 'galicaster'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=True)
    ip = db.Column(db.String(22))
    port = db.Column(db.Integer)
    user = db.Column(db.String(20))
    asset_of = db.Column(db.Integer, db.ForeignKey('users.id'))

    def __init__(self, name, ip, port, user):
        self.name = name
        self.ip = ip
        self.port = port
        self.user = user

    def __repr__(self):
        return '<unit: %r>' % (self.name)


class Role(db.Model):
    __tablename__ = 'roles'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    users = db.relationship('User', backref='role', lazy='dynamic')

    def __repr__(self):
        return '<Role %r>' % self.name

    @staticmethod
    def insert_roles():
        roles = {
            'User': (Permission.CONTROL_REC, True),
            'Adv. User': (Permission.CONTROL_REC | Permission.REBOOT, False),
            'Moderator': (Permission.MODERATOR, False),
            'Administrator': (Permission.ADMIN, False)
        }
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.permissions = roles[r][0]
            db.session.add(role)
        db.session.commit()


class User(UserMixin, db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, index=True)
    email = db.Column(db.String(64), index=True)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    password_hash = db.Column(db.String(128))
    user_assets = db.relationship(
        'Galicaster', secondary=assets, backref=db.backref('assets', lazy='dynamic'))

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        if self.role is None:
            if self.email is app.config['ADMIN_EMAIL']:
                self.role = Role.query.filter_by(permissions=0xff).first()
            else:
                self.role = Role.query.filter_by(default=True).first()

    @property
    def password(self):
        raise AttributeError('Password may not be read!')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def get_assets(self):
        return self.user_assets

    # Start of role permissions
    def can(self, permissions):
        return self.role is not None and (self.role.permissions & permissions) == permissions

    def is_administrator(self):
        return self.can(Permission.ADMIN)

    def is_moderator(self):
        return self.can(Permission.MODERATOR)

    def is_adv_user(self):
        return self.can(Permission.CONTROL_REC | Permission.REBOOT)

    def __repr__(self):
        return '<User %r>' % self.username


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)
