from flask import (session, render_template, redirect, url_for,
                   flash, request, abort)
from flask_login import login_required, current_user
from subprocess import (Popen, check_output, CalledProcessError)
from datetime import datetime
from re import compile, search
from json import loads as load_json
import requests


from . import main
from .. import db
from ..decorators import moderator_required, permission_required
from ..models import Galicaster, Permission


def get_free_space_root(raw_data):
    """Evaluates a (multiline) String to find the free space on "/" (root).

    Args:
        raw_data (String): The raw data provided by "df -h" on host.

    Returns:
        String: Available space on root partition of host.
    """
    df_regex = "(\d*.)\s*(\d*%)\s/\n"
    pattern = compile(df_regex)
    df = pattern.search(raw_data)
    if df is None:
        return "Error"
    else:
        return df.group(1)


def get_free_space_pub(raw_data):
    """Evaluates a (multiline) String to find the free space on "/pub" (galicaster repository).

    Args:
        raw_data (String): The raw data provided by "df -h" on host.

    Returns:
        String: Available space on repository partition/folder of host.
    """
    df_regex = "(\d*.)\s*(\d*%)\s/pub\n"
    pattern = compile(df_regex)
    df = pattern.search(raw_data)
    if df is None:
        return "Error"
    else:
        return df.group(1)


def setPermissionsInSession():
    """Sets permission based on session cookie entry.

    Returns:
        None: Permissions are set for current user.
    """
    if current_user.is_anonymous:
        session['is_admin'] = False
        session['is_mod'] = False
        session['is_adv_user'] = False
    else:
        session['is_admin'] = current_user.is_administrator()
        session['is_mod'] = current_user.is_moderator()
        session['is_adv_user'] = current_user.is_adv_user()


def ip(name):
    """Returns IPv4 address from name of entry

    Args:
        name (String): The name of the entry, e.g. "Galicaster01".

    Returns:
        String: IPv4 representation.
    """
    return session['list'][name]['ip']


def ip_port(name):
    """Returns port from name of entry.

    Args:
        name (String): The name of the entry, e.g. "Galicaster01".

    Returns:
        String: Port as string.
    """
    return session['list'][name]['ip'] + ':' + \
        str(session['list'][name]['port'])


def read_sessionList():
    session.pop('list', None)
    assets = Galicaster.query.all()
    tmp_dict = dict()
    # test = current_user.get_assets()
    for asset in assets:
        tmp_dict[asset.name] = {
            'ip': asset.ip, 'port': asset.port, 'user': asset.user}

    session['list'] = tmp_dict
    del tmp_dict


def sendSig(sig, name):
    try:
        sentSignal = requests.get(sig)
    except requests.exceptions.Timeout as e:
        flash("Exception: %s" % e)
        session['list'][name]['state'] = 'timeout'
    except requests.exceptions.RequestException as e:
        flash("Exception: %s" % e)
    else:
        if sentSignal is None:
            flash("Caught Exception: %s" % e)
            read_sessionList()
            return redirect(url_for('main.index'))
        else:
            return sentSignal.text


@main.route('/', methods=['GET', 'POST'])
def index():
    setPermissionsInSession()

    if 'list' not in session:
        try:
            read_sessionList()
        except:
            flash('Added initial entry to configuration.')
            if 'list' not in session:
                test = Galicaster("TEST", "10.10.10.10", 8080, "galicaster")
                db.session.add(test)
                db.session.commit()
                read_sessionList()
        else:
            if 'list' in session:
                return abort(500)

    if 'last_update' not in session:
        session['last_update'] = datetime.utcnow()

    return render_template('dashboard.html', gc_list=session['list'])


@main.route('/getscreen', methods=['POST'])
def getscreen():
    if request.method == 'POST':
        name = request.form.get('screen')
        session['screenshot_name'] = name
        session['screenshot_ip'] = ip_port(name)

        # read_sessionList()
        return redirect(url_for('main.index'))


@login_required
@main.route('/refresh', methods=['POST'])
def refresh():
    if 'screenshot_name' in session:
        session.pop('screenshot_name')
        session.pop('screenshot_ip')

    if request.method == 'POST':

        name = request.form.get('refresh')

        """ check if all units need to be refreshed """
        if name == "all":
            read_sessionList()
            work_on = session['list']
        else:
            work_on = [name]

        for name in work_on:
            url = 'http://' + ip_port(name) + '/state'
            try:
                data = load_json(requests.get(url).content)
            except requests.exceptions.Timeout as e:
                flash("Exception: %s" % e)
                session['list'][name]['state'] = 'timeout'
            except requests.exceptions.RequestException as e:
                flash("Exception: %s" % e)
                session['list'][name]['state'] = 'error'
            else:
                if data is None:
                    flash("No data received from %s" % name)
                else:
                    session['list'][name]['state'] = data['is-recording']

            """ get free space left on '/' """
            cmd = ['ssh', session['list'][name]['user'] + '@' +
                   ip(name), 'df -h']
            df = None
            try:
                df = check_output(cmd)
            except CalledProcessError as e:
                flash('Failed with: \"%s\"' % e)

            if df is None:
                flash("Error on getting free space with: %s" % name)
            else:
                session['list'][name]['free_space_root'] = get_free_space_root(df)
                session['list'][name]['free_space_pub'] = get_free_space_pub(df)


        """ If single entry refreshed, get latest screenshot """
        session['last_update'] = datetime.utcnow()
        if len(work_on) == 1:
            session['screenshot_name'] = name
            session['screenshot_ip'] = ip_port(name)
            flash('Entry %s refreshed.' % name)
        else:
            flash('All entries refreshed.')

        return redirect(url_for('main.index'))


@main.route('/reboot', methods=['POST'])
@login_required
@permission_required(Permission.REBOOT)
def reboot_gc():
    if request.method == 'POST':
        name = request.form.get('reboot')
        cmd = ['ssh', session['list'][name][
            'user'] + '@' + ip(name), 'sudo reboot']
        try:
            Popen(cmd)
        except CalledProcessError as e:
            flash(e)
        else:
            flash('Rebooting %s!' % name)

    if ('screenshot_name' in session) and (name == session['screenshot_name']):
        session.pop('screenshot_name')
        session.pop('screenshot_ip')

    return redirect(url_for('main.index'))


@main.route('/clearlog', methods=['POST'])
@login_required
@permission_required(Permission.REBOOT)
def clearlog():
    if request.method == 'POST':
        name = request.form.get('clearlog')
        cmd = ['ssh', session['list'][name]['user'] +
               '@' + ip(name), 'rm ~/.xsession-errors*']
        try:
            Popen(cmd)
        except CalledProcessError as e:
            flash(e)
        else:
            flash('Log cleared on %s.' % name)

    return redirect(url_for('main.index'))


@main.route('/recording/start', methods=['POST'])
@login_required
@permission_required(Permission.CONTROL_REC)
def startRecording():
    if request.method == 'POST':
        name = request.form.get('rec_start')
        url = 'http://' + ip_port(name) + '/start'
        sendSignal = sendSig(url, name)
        flash('Recording signal (start) sent to: %s\n got: %s' %
              (name, sendSignal))
    else:
        flash("Only POST requests allowed.")
    return redirect(url_for('main.index'))


@main.route('/recording/stop', methods=['POST'])
@login_required
@permission_required(Permission.CONTROL_REC)
def stopRecording():
    if request.method == 'POST':
        name = request.form.get('rec_stop')
        url = 'http://' + ip_port(name) + '/stop'
        # sendSignal = requests.get(url)
        sendSignal = sendSig(url, name)
        flash('Recording signal (stop) sent to: %s\n got: %s' %
              (name, sendSignal))
    else:
        flash("Only POST requests allowed.")
    return redirect(url_for('main.index'))


@main.route('/config', methods=['GET', 'POST'])
@login_required
@moderator_required
def showConfig():
    setPermissionsInSession()

    if request.method == 'GET':
        return render_template('configuration.html', gc_list=session['list'])


@main.route('/config/add', methods=['POST'])
@login_required
@moderator_required
def addConfig():
    setPermissionsInSession()

    if request.method == 'POST':
        ip = request.form.get('host')
        port = request.form.get('port')
        user = request.form.get('username')
        name = request.form.get('name')

        """ add values to db """
        try:
            new_item = Galicaster(name=name, ip=ip, port=port, user=user)
            db.session.add(new_item)
            db.session.commit()
            flash("Entry added...")
            read_sessionList()
        except Exception as e:
            flash("Error: %s" % e)

        return redirect(url_for('main.showConfig'))
    else:
        flash("Request Method not allowed!")
        return render_template('configuration.html', gc_list=session['list'])


@main.route('/config/del', methods=['POST'])
@login_required
@moderator_required
def delConfig():
    setPermissionsInSession()

    if request.method == 'POST':
        name = request.form.get('remove')
        obj_id = Galicaster.query.filter_by(name=name).first().id
        obj = Galicaster.query.get(obj_id)
        try:
            db.session.delete(obj)
            db.session.commit()
            flash("Succesfully removed entry for \"%s\" from db." % name)
            read_sessionList()
            return redirect(url_for('main.showConfig'))
        except:
            flash("Unknown Exception!")
    else:
        flash("Request Method not allowed!")
        return render_template('configuration.html', gc_list=session['list'])
